'use strict';
const { exec } = require("child_process");
const fs = require('fs');
let lastEpoch = 209;
let epoch = 0;
// Remote MySQL DB credentials
let mysql      = require('mysql');
let connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'solana',
  database : 'solana',
  multipleStatements: true
});

// Value formatting to SQL propper inputs
let valuesToSql = function(input,param) {
  let value = input;
  if (util.isUndefined(value)) { value = null};
  let parameter = param;
  let formatedVal;
  // Match datetime values and convert
  dateRegex = /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})\.(\d{3})(\+|\-)(\d{2})\:(\d{2})/;
  if (parameter=='dateonly') {
    formatedVal = "CONVERT('"+value+"',date)";
  // Match string values to add ''
  } else if (dateRegex.test(value)) {
    formatedVal = "CONVERT('"+value+"',datetime)";
  // Match string values to add ''
} else if(typeof value == 'string' || parameter == 'string') {

    formatedVal = value.replace(/("|')/g, "");

  // Replace reserved characters
  } else  {
    formatedVal = value;
  }
  //return value;
  if (formatedVal == null) {
    formatedVal = value;
  };

  return formatedVal;
}


let runShell = function(command, callback){
    let cmd = command;
    console.log("Iniciando runShell con comando "+cmd);

    exec(cmd, (error, stdout, stderr) => {
        if (error) {
            //console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            //console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        //epoch = stdout;
        callback(error,stdout);

    });
};

let processDelegators = function(epoch, err, res) {
    let ep = epoch;
    let delegatorsJSON = './delegators'+ep+'.json';
    let rawdata = fs.readFileSync(delegatorsJSON);

    let validatorsArray = new Array();
    let delegatorsArray = new Array();
    let validatorsData = {};
    let delegators = JSON.parse(rawdata);
    let validatorsJSON = './validators'+ep+'.json'
    let validators = JSON.parse(fs.readFileSync(validatorsJSON));
    // Loop through validators
    for (const keyv in validators.validators) {
        // Loop through delegators to find mathes w/validators and populate array for mysql insertion

        for (const keyd in delegators) {
            if (delegators[keyd].delegatedVoteAccountAddress == validators.validators[keyv].voteAccountPubkey) {
                // When a match is found check if (Q) delegators property is present
                if (validators.validators[keyv].hasOwnProperty('delegators')) {
                    console.log("Validators JSON ya tiene delegators property");
                    // Add one to the validator's delegators count 
                    validators.validators[keyv].delegators = validators.validators[keyv].delegators + 1;
                } else {
                    console.log("Creando delegators property en Validators JSON");
                    validators.validators[keyv].delegators = 1;
                };
                // Check for Max Stake property
                if (validators.validators[keyv].hasOwnProperty('maxStake')) {
                    // Check if current Max value is below current delegators stake
                    if (validators.validators[keyv].maxStake < delegators[keyd].delegatedStake) {
                        validators.validators[keyv].maxStake = delegators[keyd].delegatedStake;
                    }
                }  else {
                    validators.validators[keyv].maxStake = delegators[keyd].delegatedStake;
                }
                // Check for Min Stake property
                if (validators.validators[keyv].hasOwnProperty('minStake')) {
                   // Check if current Min value is below current delegators stake
                    if (validators.validators[keyv].minStake > delegators[keyd].delegatedStake) {
                        validators.validators[keyv].minStake = delegators[keyd].delegatedStake;
                    }
                }  else {
                    validators.validators[keyv].minStake = delegators[keyd].delegatedStake;
                }

                //console.log("Validator "+validators.validators[keyv].voteAccountPubkey+" has "+validators.validators[keyv].delegators+" Delegators");
                //console.log(validators.validators[keyv]);
            };
            /*
            let values = [epoch, delegators[keyd].stakePubkey , delegators[keyd].stakeType , delegators[keyd].accountBalance , delegators[keyd].creditsObserved , delegators[keyd].delegatedStake , delegators[keyd].delegatedVoteAccountAddress , delegators[keyd].activationEpoch , delegators[keyd].staker , delegators[keyd].withdrawer , delegators[keyd].rentExemptReserve , delegators[keyd].activeStake];
            let query = "INSERT INTO delegators (epoch,stakePubkey,stakeType,accountBalance,creditsObserved,delegatedStake,delegatedVoteAccountAddress,activationEpoch,staker,withdrawer,rentExemptReserve,activeStake) VALUES ? ;";
            console.log(keyd);
            console.log(values);
            connection.query(query , values, function(err, rows, fields) {
                console.log("insertando delegators");
                if (err) throw err;
                console.log('MySQL result: ',err,rows,fields);
                
            });
            */

            //delegatorsArray.push([ epoch, delegators[keyd].stakePubkey , delegators[keyd].stakeType , delegators[keyd].accountBalance , delegators[keyd].creditsObserved , delegators[keyd].delegatedStake , delegators[keyd].delegatedVoteAccountAddress , delegators[keyd].activationEpoch , delegators[keyd].staker , delegators[keyd].withdrawer , delegators[keyd].rentExemptReserve , delegators[keyd].activeStake ]);
        };
        //validatorsArray.push([ epoch, validators.validators[keyv].identityPubkey, validators.validators[keyv].voteAccountPubkey, validators.validators[keyv].commission , validators.validators[keyv].lastVote , validators.validators[keyv].rootSlot, validators.validators[keyv].credits, validators.validators[keyv].epochCredits, validators.validators[keyv].activatedStake, validators.validators[keyv].version, validators.validators[keyv].delinquent, validators.validators[keyv].skipRate ]);

    };



    //console.log("Validators Data for insertion: "+validatorsArray);
    //console.log("Delegators Data for insertion:"+delegatorsArray);
    let outputFile = './output'+ep+'.json'
    fs.writeFile(outputFile, JSON.stringify(validators), function(err, res){
        console.log(err,res);
    });



};

runShell('solana epoch --output json',function(err,res){
    if (err) {
        console.log(err);
    }   else {
        //epoch = res;
        epoch = Number(res);
        console.log("Me devolvió el epoch: "+epoch);

        if (lastEpoch < epoch) {
            console.log("Estamos en un nuevo Epoch");
            let validatorsReq = "solana validators --output json > ./validators"+epoch+".json";
            runShell(validatorsReq,function(err,res){
                console.log("Iniciando runShell para solicitud de validadores");
                if (err) {
                    console.log("Error al buscar Validadores"+err);
                }   else {
                    console.log("No hubo errores al buscar validadores");
                    console.log(res);
                }
            });
            let delegatorsReq = "solana stakes --output json > ./delegators"+epoch+".json";
            runShell(delegatorsReq,function(err,res){
                console.log("Iniciando runShell para solicitud de delegadores");
                if (err) {
                    console.log("Error al buscar Delegadores"+err);
                }   else {
                    console.log("No hubo errores al buscar delegadores");
                    console.log(res);
                    processDelegators(epoch);
                }
            });            
        } else {
            console.log("Seguimos en el Epoch #"+lastEpoch);

        };        
    }
});





